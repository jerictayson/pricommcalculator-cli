import java.util.Scanner;
public class SimplePricommProgram{

        /*
            
            This code is a mess right now but i will gonna improved it
            Created by : Jeric Tayson.
            I created this program for my guide to my computations problems, and also for fun.
            
            Bugs:
            Some Floating point numbers dont get to decimals for some reason. -> Fixed.
            Incomplete try/catch block of every user input.(it will only occur if user the user dont know how to follow instructions) -> fixed some of it
            NF Decibels giving decimal point instead of whole numbers -> it turns out that is pretty normal, (I think)
            Some Variables is not properly aligned.(because double type return function has access to the global variables) need only to expose to check the selectionValues for user input -> fixed
            Some SIUnits in toHertz function has same first character -> Fixed*
            Some negative numbers  dont go with the first whole number -> Fixed
            Due to some very long scientific notation some floating point can have (3) three decimal places on their output
            Some decimal formatting is not fully implemented yet, i will fix it para sayo yieee...
        */
        
        //these global variable declarations is used for main and its subroutines
        private static String[] SIunits = {"AttoHertz", "FemtoHertz", "PicoHertz", "NanoHertz", "MicroHertz", "MilliHertz", "CentiHertz", "DeciHertz", "DecaHertz", "HectoHertz", "KiloHertz", "MegaHertz", "GigaHertz", "TeraHertz", "PetaHertz", "ExaHertz"};
        static char choice = 'y';
        final static double K = 1.38 * Math.pow(10, -23), I = 1.6 * Math.pow(10,-19);
        static Scanner scanner = new Scanner(System.in);
        
        //Driver code
        public static void main(String args[]){
        double finalAnswer = 0.0, frequency = 0.0, hertz = 0.0, temperature = 0.0, resistance = 0.0, voltageNoise = 0.0, voltageSignal = 0.0, current = 0.0, noiseRatio = 0.0, maxSignal = 0.0, minSignal = 0.0,
        carrierFrequency = 0.0, modulatingFrequency = 0.0, frequencyDeviation = 0.0, modulationFrequency = 0.0, deviationSensitivity = 0.0, peakMaxVoltage = 0.0, actualDeviationFrequency = 0.0, maximumDeviationFrequency = 0.0;
        char tryAgain = 'y';
        int selection = 0;
        String data = "";// should i use StringBuffer? because Strings are immutable and cannot change its value only the referrence of the variable to the new String literal to it or object instantiation.
            do{
                //this reinalization is for removing the existing data from the variables to avoid unexpected output when user wants to restart the program again.
                finalAnswer = 0.0;
                frequency = 0.0; 
                hertz = 0.0;
                temperature = 0.0;
                resistance = 0.0;
                voltageNoise = 0.0; 
                voltageSignal = 0.0;
                current = 0.0; 
                noiseRatio = 0.0;
                maxSignal = 0.0; 
                minSignal = 0.0;
                carrierFrequency = 0.0; 
                modulatingFrequency = 0.0;
                frequencyDeviation = 0.0;
                modulationFrequency = 0.0;
                deviationSensitivity = 0.0;
                peakMaxVoltage = 0.0;
                actualDeviationFrequency = 0.0;
                maximumDeviationFrequency = 0.0;
                data = "";
                //it seems like dumb but this reinalization resets the value of the variable to the next program operations.

                try {
                    
                    do{
                        //check for Computation Selection
                        try{
                         System.out.print("What do you want to compute:\n\n\n\n1.Thermal Voltage Noise\n2.Thermal Power Noise\n3.Shot Noise"+
                         "\n4.Signal to Noise Ratio (Volts)\n5.Signal to Noise Ratio (Watts) and Noise Figure -> NF"+
                         "\n6.Noise Temperature\n7.Modulating Index\n8.SideBands\n"+
                         "9.Overall Noise Performance\n\n\n\nAngle Modulations\n\nFM Systems\n10.Modulating Index "+
                         "(mf)\n11.Frequency Deviation("+"\u0394".toLowerCase()+")\n12.Bandwidth (BW)\n\n\nPhase Systems\n13.Modulation Index (m)"+
                         "\n14.Frequency Deviation\n\n\n\nFM and PM System\n"+
                         "15.Percentage Modulation (%M)\n16.Deviation Ration (DR)\nEnter your selected computation:");
                         selection = scanner.nextInt();
                         if(selection<=16 & selection>0)
                         break;
                        
                         System.out.println("Try again.");
                        
                        }catch(Exception e){
                        System.out.println("Invalid user input\nCheck your input!!\n\n");
                        e.printStackTrace();
                        scanner.nextLine();
                        }
                        }while(true);
                        
                        
                        //check wether what computation user wants
                        if(selection ==  1){
                        
                        //Thermal Voltage Noise
                        do{
                        
                            try{
                            
                            System.out.println("You Selected Computation is Thermal Voltage Noise\n"
                        +"The Formula of this Computation is Vn = sqrt(4KTBR)\nWhere:\nVn is the rms noise Voltage\nk is the Boltzman's Constant\n"+
                        "T is the Temperature (K)\nB is the Bandwidth (Hz)\nR is the resistance\nEnter the Given\\s based of the given question");
                        System.out.print("What is Temperature:");
                        temperature = scanner.nextDouble();
                        System.out.print("Do you need conversion?:[y/n]");
                        char choice = scanner.next().charAt(0);
                        if(choice == 'y'){
                        
                        System.out.print("Is this Celcius? Type C, otherwise Type F to Farenheit:");
                        choice = scanner.next().toUpperCase().charAt(0);
                        temperature = tempConversion(choice ,temperature);
                        }else
                        System.out.println("Skipping...");
                        
                        System.out.print("Enter the given Bandwidth:");
                        frequency = scanner.nextDouble();
                        System.out.print("Do you need conversion?:[y/n]");
                        choice = scanner.next().charAt(0);
                        if(choice == 'y'){
                        
                        hertz = hertzConversion(selection, frequency);
                        }else{
                        System.out.println("Skipping...");
                        hertz = frequency;
                        }
                        
                        
                        System.out.print("Enter the given resistance:");
                        resistance = scanner.nextDouble();
                        
                        finalAnswer = computeInternalVoltageNoise(temperature, hertz, resistance);
                        data = String.valueOf(finalAnswer);
                        System.out.println("The answer is:"+ finalAnswer);
                        System.out.println("Generating verbose answer Please Wait");
                        try{
                        
                            Thread.sleep(1000);
                        
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        System.out.println("Given\\s \nTemperature : "+ temperature+ "\nBandwidth : "+ hertz+"\nResistance : "+resistance);//im having trouble using prinf function because of floating point numbers..
                        if(data.contains("E"))
                        System.out.printf("Formula : Vn = sqrt(4KTBR) \nFinal Answer : %.3gVn%n%s%n", finalAnswer,"e or E means x10^(n)");
                        else
                        System.out.printf("Formula : Vn = sqrt(4KTBR) \nFinal Answer : %.2g Vn%n", finalAnswer);
                        break;
                            
                            
                            }catch(Exception e){
                            System.out.println("\n\n\nPlease Enter a correct input");
                            scanner.nextLine();
                            e.printStackTrace();
                            }
                        
                        
                        }while(true);
                        
                        //Selection No.2
                        }else if(selection == 2){
                        
                        do{
                        
                            try{
                            
                            //Thermal Power Noise
                        System.out.println("You Selected Computation is Thermal Power Noise\n"
                        +"The Formula of this Computation is Pn = KTB\nWhere:\nPn is the average noise voltage (watts)\n"+
                        "k is the Boltzman constant\nT is the Temperature (K)\nB is the Bandwidth (Hz)Enter the Given\\s based of the given question");
                        System.out.print("What is Temperature:");
                        temperature = scanner.nextDouble();
                        System.out.print("Do you need conversion?:[y/n]");
                        char choice = scanner.next().charAt(0);
                        if(choice == 'y'){
                        
                        System.out.print("Is this Celcius? Type C, otherwise Type F to Farenheit:");
                        choice = scanner.next().toUpperCase().charAt(0);
                        temperature = tempConversion(choice ,temperature);//can i use the function with return type with included selection mode for the user?? -,- hmmm.
                        }else
                        System.out.println("Skipping...");
                        
                        System.out.print("Enter the given Bandwidth:");
                        frequency = scanner.nextDouble();
                        System.out.print("Do you need conversion?:[y/n]");
                        choice = scanner.next().toUpperCase().charAt(0);
                        if(choice == 'Y'){
                        
                        hertz = hertzConversion(selection, frequency);
                        }else{
                        hertz = frequency;
                        System.out.println("Skipping...");
                        }
                        
                        
                        finalAnswer = computeInternalPowerNoise(temperature,  hertz);
                        data = String.valueOf(finalAnswer);
                        System.out.println("The answer is:"+ finalAnswer);
                        System.out.println("Generating verbose answer Please Wait");
                        try{
                        
                           Thread.sleep(1000);
                        
                        }catch(Exception e){}
                        System.out.printf("Given\\s \nTemperature : %f\nBandwidth : %.3g%n",temperature, hertz);
                        if(data.contains("E"))
                        System.out.printf("Formula : Pn = KTB Final Answer : %.3g W%n%s%n", finalAnswer,"e or E means x10^(n)");
                        else 
                        System.out.printf("Formula : Pn = KTB Final Answer : %.2f%n", finalAnswer);
                        break;
                            }catch(Exception ex){
                            
                            scanner.nextLine();
                            System.out.println("\n\n\nPlease Enter a correct input");
                            ex.printStackTrace();
                            
                            }
                        
                        }while(true);
                         
                        
                        }else if(selection == 3){
                        
                        do{
                        
                            try{
                            
                            System.out.println("You Selected Computation is Shot Noise\n"
                        +"The Formula of this Computation is In = sqrt(2qIB)\nWhere In is the rms noise current\n"+
                        "q is the charge of an electron\n I is the direct current (A)\nB is the Bandwidth (Hz)\nEnter the Given\\s based of the given question");
                        
                        System.out.print("Enter the given current:");
                        current = scanner.nextDouble();
                         System.out.print("Do you need conversion?:[y/n]");
                        choice = scanner.next().charAt(0);
                        if(choice == 'y'){
                        
                         /*
                        It is a compatible with the watts conversion
                        because they have same SI Units.
                        */
                        current = hertzConversion(selection, current);
                        }else
                        System.out.println("Skipping...");
                        selection = 1;
                        System.out.print("Enter the given Bandwidth:");
                        frequency = scanner.nextDouble();
                        System.out.print("Do you need conversion?:[y/n]");
                        choice = scanner.next().charAt(0);
                        if(choice == 'y'){
                        
                        hertz = hertzConversion(selection, frequency);
                        }else{
                        hertz = frequency;
                        System.out.println("Skipping...");
                        }
                        
                        
                        finalAnswer = computeShotNoise(current,  hertz);
                        data = String.valueOf(finalAnswer);
                        System.out.println("The answer is:"+ finalAnswer);
                        System.out.println("Generating verbose answer Please Wait");
                        try{
                        
                           Thread.sleep(1000);
                        
                        }catch(Exception e){
                
                            e.printStackTrace();
                        }
                        
                        System.out.println("Given\\s \nCurrent : "+ current+ "\nBandwidth : "+ hertz);
                        if(data.contains("E"))
                        System.out.printf("Formula : In = sqrt(2qIB) Final Answer : %.3g In%n%s%n", finalAnswer,"e or E means x10^(n)");
                        else
                        System.out.printf("Formula : In = sqrt(2qIB) Final Answer : %.2f In%n", finalAnswer);
                        break;
                            
                            }catch(Exception e){
                            
                            scanner.nextLine();
                            System.out.println("\n\n\nPlease Enter a correct input");
                            e.printStackTrace();
                            }
                        
                        
                        }while(true);
                        
                        
                        }else if(selection == 4 | selection == 5){
                        
                        do{
                        
                            try{
                            
                            System.out.println("The selected computation is SNR = Vs/Vn\nSNR = Ps/Pn -> Power SNR\nNR = SNRinput/SNROutput -> Noise Ratio\nNF = 10log(NR) -> Noise Figure");
                            System.out.println("Where:\nSNR is the Signal to Noise Ration\nVs is the Voltage Signal\nVn is the Voltage Noise\nPn is the Power Noise\nPs is the Power Signal");
                            System.out.println("NR is the Noise Ratio\nNF is the noise Figure (dB)");
                            System.out.println("Enter the given\\s in the question.\nNote: Depends on your selected computation, logarithm equation depends on your selection\n 20log() -> 4 (Voltage)\n 10log() -> 5 SNR (Watts\\Power)");
                            System.out.print("Enter Voltage\\Power Signal or SNR input:");
                            voltageSignal = scanner.nextDouble();

                            char choice1 = 'n';

                            System.out.print("Do you need conversion?:[y/n]");
                            choice1 = scanner.next().charAt(0);
                            if(choice1 == 'y')
                                voltageSignal = hertzConversion(11, voltageSignal);

                            System.out.print("Enter Voltage\\Power Noise or SNR output:");
                            voltageNoise = scanner.nextDouble();
                            System.out.println(voltageNoise);
                            
                            System.out.print("Do you need conversion?:[y/n]");
                            choice1 = scanner.next().charAt(0);
                            if(choice1 == 'y')
                                voltageNoise = hertzConversion(11, voltageNoise);

                            System.out.print("Is your equation expressed in decibels?[y/n]:");
                            choice = scanner.next().charAt(0);
                            finalAnswer = computeSNR(voltageSignal, voltageNoise, selection);
                            data = String.valueOf(finalAnswer);
                            System.out.println("\n\n\nThe answer is:"+ finalAnswer);
                            System.out.println("Generating verbose answer Please Wait");
                            try{
                        
                            Thread.sleep(1000);
                        
                            }catch(Exception e){}
                            System.out.println("Given\\s \nVoltage\\Power Signal || SNR Input : "+voltageSignal+ "\nVoltage\\Power Noise || SNR Output : "+ voltageNoise);//im having trouble using prinf function because of floating point numbers..
                            if(choice == 'y'){
                                if(data.contains("E"))
                                System.out.printf("Formula : SNR = Vs/Vn Final Answer : %.3gdB%s%n", finalAnswer, "E or e Means x10^(n)");
                                else
                                System.out.printf("Formula : SNR = Vs/Vn Final Answer : %.2fdB%n", finalAnswer);
                            }
                            else{
                                if(data.contains("E"))
                                System.out.printf("Formula : SNR = Vs/Vn Final Answer : %.2fSNR%n", finalAnswer);
                                else
                                System.out.printf("Formula : SNR = Vs/Vn Final Answer : %.3gSNR%n", finalAnswer);
                            }
                            break;
                            }catch(Exception e){
                            
                            scanner.nextLine();
                            System.out.println("\n\n\nPlease Enter a correct input");
                            
                            }
                        
                        
                        }while(true);
                        
                        }else if(selection == 6){
                        
                            try{
                            
                                do{
                            
                                System.out.println("The computation of Noise Temperature are Tn = 290(NR-1)");
                                System.out.print("Enter the Noise ratio of your equation:");
                                noiseRatio = scanner.nextDouble();
                            
                                finalAnswer = computeNoiseTemperature(noiseRatio);
                                
                                System.out.println("The Noise temperature of :"+noiseRatio+" is "+finalAnswer+"K or Kelvin");
                                break;
                                
                                }while(true);
                            
                            
                            }catch(Exception e){
                            
                            System.out.println("Invalid input try again.");
                            scanner.nextLine();
                            e.printStackTrace();
                            }
                        
                        }else if(selection == 7){
                        
                            do{
                            try{
                            
                            System.out.println("The Computation of Modulating index are m = (Vmax - Vmin)/(Vmax + vMin)");
                            System.out.print("Enter the given Max Signal:");
                            maxSignal = scanner.nextDouble();
                            System.out.print("Enter the given Min Signal:");
                            minSignal = scanner.nextDouble();
                            
                            finalAnswer = computeModulatingIndex(maxSignal, minSignal);
                            System.out.println("Generating verbose output Please Wait...");
                            try{
                            
                            Thread.sleep(2000);
                            
                            }catch(Exception e){
                            e.printStackTrace();
                            }
                            System.out.println("Vm = "+ maxSignal+"\nVc = "+ minSignal);
                            System.out.printf("Final Answer is : %.2gM%nNote: if you encounter in the answer that has Letter e in the floating point, it indicated x10^ with the next numbers%n", finalAnswer);
                            break;
                            
                            }catch(Exception e){
                            e.printStackTrace();
                            System.out.println("Invalid input try again.");
                            scanner.nextLine();
                            }
                            }while(true);
                        
                        }else if(selection == 8){
                        
                        do{
                        
                            try{
                            
                            System.out.println("The computation of sidebands are Fusb = fc + fm for upper sidebands");
                            System.out.println("Flsb = fc - fm for lower sidebands");
                            System.out.println("Where:\nFusb is the upper Sidebands\nFlsb is the lower sidebands");
                            System.out.println("Fc is the carrier frequency");
                            System.out.println("Fm is the modulating frequency");
                            System.out.print("Enter the given carrier frequency:");
                            carrierFrequency = scanner.nextDouble();
                            System.out.print("Enter the modulatingFrequency:");
                            modulatingFrequency = scanner.nextDouble();
                            System.out.print("Press 1 if the Fusb or Press 2 Flsb:");
                            choice = scanner.next().charAt(0);
                            if(choice == '1' | choice == '2')
                            finalAnswer = computeSideBands(carrierFrequency, modulatingFrequency, choice);
                            System.out.println("Generating verbose output Please Wait...");
                            try{
                            
                            Thread.sleep(2000);
                            
                            }catch(Exception e){
                            e.printStackTrace();
                            
                            }
                            System.out.println("Fc = "+ carrierFrequency+"\nFm = "+ modulatingFrequency);
                            System.out.printf("Final Answer is : %.3g%nNote: if you encounter in the answer that has Letter e in the floating point, it indicated x10^ with the next numbers%n", finalAnswer);
                            break;
                            }catch(Exception e){
                            e.printStackTrace();
                            scanner.nextLine();
                            System.out.println("Invalid input try again...");
                            }
                        
                        }while(true);
                        
                        }else if(selection == 9){
                        
                        finalAnswer = computeOverallNF();
                        data = String.valueOf(finalAnswer);
                        System.out.printf("The Answer is NRT :%.2f%n", finalAnswer);
                        System.out.print("Do you want to dB output?[y/n]:");
                        choice = scanner.next().toLowerCase().charAt(0);
                        if(choice == 'y')
                            System.out.printf("10log(%.2f) = %.2fdB%n", finalAnswer, (double)(Math.log10(finalAnswer)*10));
                        }else if(selection == 10){

                            do{

                                try {
                                    System.out.println("\n\nThe selected computation is Modulating index of an FM Signal.");
                                    System.out.println("The Formula of this computation is mf = fd/fm");
                                    System.out.println("Where:\nmF is the Modulating Frequency.\nfd is the Frequency Deviation\nfm = Modulating Frequency");
                                    System.out.print("Enter the Frequency Deviation:");
                                    frequencyDeviation = scanner.nextDouble();

                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    frequencyDeviation = hertzConversion(selection, frequencyDeviation);
                                    System.out.print("Enter the Modulation Frequency:");
                                    modulationFrequency = scanner.nextDouble();

                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    modulationFrequency = hertzConversion(selection, modulationFrequency);
                                    finalAnswer = computeFMModulatingIndex(frequencyDeviation, modulationFrequency);
                                    data = String.valueOf(finalAnswer);
                                } catch (Exception e) { 
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                                    if(data.contains("E"))
                                        System.out.printf("The Answer is %.3g mf%n", finalAnswer);
                                        else
                                        System.out.printf("The answer is %.2f mf%n", finalAnswer);
                                    break;
                            }while(true);
                        }else if(selection == 11){

                            do{

                                try {
                                    System.out.println("\n\nThe selected computation is Frequency Deviation of an FM Signal.");
                                    System.out.printf("The Formula of this computation is %s = k2*Vm%n", "\u0394".toLowerCase());
                                    System.out.println("Where:\n"+"\u0394".toLowerCase()+" is the Frequency Deviation (Hz)\nk2 is the deviation sensitivity(rad/s.V)\nVm is the peak Modulating Signal (Hz)");
                                    System.out.print("Enter the Deviation Sensitivity:");
                                    deviationSensitivity = scanner.nextDouble();

                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    deviationSensitivity = hertzConversion(selection, deviationSensitivity);
                                    System.out.print("Enter the Peak Modulating Signal:");
                                    peakMaxVoltage = scanner.nextDouble();
                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    peakMaxVoltage = hertzConversion(selection, peakMaxVoltage);
                                    finalAnswer = computedeviationSensitivity(deviationSensitivity, peakMaxVoltage);
                                    data = String.valueOf(finalAnswer);
                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                                if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g %s%n", finalAnswer, "\u0394".toLowerCase());
                                    else
                                    System.out.printf("The answer is %.2f %s%n", finalAnswer, "\u0394".toLowerCase());
                                break;
                            }while(true);

                        }else if(selection == 12){

                            do {
                                try {
                                    System.out.println("\n\nThe selected computation is the Bandwidth of an FM Signal.");
                                    System.out.println("The Formula of this computation is BW = 2[fd(max) + fm(max)]");
                                    System.out.println("Where:\nBW is the bandwidth\nfd(max) is the maximum frequency deviation\nfm(max)is the maximum modulating frequency");
                                    
                                    System.out.print("Enter the frequency deviation:");
                                    frequencyDeviation = scanner.nextDouble();
                                    
                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    frequencyDeviation = hertzConversion(selection, frequencyDeviation);
                                    
                                    System.out.print("Enter the modulating frequency:");
                                    modulationFrequency = scanner.nextDouble();
                                    
                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    modulationFrequency = hertzConversion(selection, modulationFrequency);

                                    finalAnswer = computeFMBandwidth(frequencyDeviation, modulationFrequency);
                                    data = String.valueOf(finalAnswer);
                                    if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g BW%n", finalAnswer);
                                    else
                                    System.out.printf("The answer is %.2f BW%n", finalAnswer);
                                    break;
                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                               
                            } while(true);
                        }else if(selection == 13){

                            do {
                                try {
                                    
                                    System.out.println("\n\nThe selected computation is the Modulation Index of a Phase System.");
                                    System.out.println("The computation of this formula is m = "+ "\u0394"+"\u03B8".toLowerCase()+" = k1*Vm");
                                    System.out.println("Where:\nm = "+"\u0394"+"\u03B8".toLowerCase()+" = is the modulation index or peak phase deviation (rad)");
                                    System.out.println("k1 is the deviation sensitivity (rad/V)");
                                    System.out.println("Vm is the peak modulating signal amplitutude (V)");
                                    
                                    System.out.print("Enter the deviation sensitivity:");
                                    deviationSensitivity = scanner.nextDouble();
                                    
                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    if(choice == 'y')
                                    deviationSensitivity = hertzConversion(selection, deviationSensitivity);
                                    
                                    System.out.print("Enter peak modulating signal amplitude:");
                                    peakMaxVoltage = scanner.nextDouble();
                                    
                                    System.out.print("Do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    peakMaxVoltage = hertzConversion(selection, peakMaxVoltage);

                                    finalAnswer = computePhaseModulatingFrequency(deviationSensitivity, peakMaxVoltage);
                                    data = String.valueOf(finalAnswer);
                                    if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g m%n", finalAnswer);
                                    else
                                    System.out.printf("The answer is %.2f m%n", finalAnswer);
                                break;
                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");

                                }
                               
                            } while (true);

                        }else if(selection == 14){

                            do {
                                
                                try {
                                    System.out.println("\n\nThe selected computation is the Frequency Deviation of a Phase System.");
                                    System.out.println("The Formula of this computation is "+"\u0394".toLowerCase()+" = k1VmFm");
                                    System.out.println("Where:");
                                    System.out.println("\u0394".toLowerCase()+" is the frequency deviation (Hz)");
                                    System.out.println("k1 is the deviation sensitivity of of PM waveform (Hz)");
                                    System.out.println("Vm is the peak modulating signal amplitude (Hz)");
                                    System.out.println("fm is the modulating signal frequency (Hz)");
                                    
                                    System.out.print("Enter the deviation sensitivity:");
                                    deviationSensitivity = scanner.nextDouble();
                                    
                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    deviationSensitivity = hertzConversion(selection, deviationSensitivity);
                                    System.out.print("Enter the peak modulating signal amplitude:");
                                    peakMaxVoltage = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    peakMaxVoltage = hertzConversion(selection, peakMaxVoltage);

                                    System.out.print("Enter the modulating frequency:");
                                    modulationFrequency = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    modulationFrequency = hertzConversion(selection, modulationFrequency);

                                    finalAnswer = computedeviationSensitivity(deviationSensitivity, peakMaxVoltage, modulationFrequency);
                                    data = String.valueOf(finalAnswer);
                                    if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g %s%n", finalAnswer, "\u0394".toLowerCase());
                                    else
                                    System.out.printf("The answer is %.2f %s%n", finalAnswer, "\u0394".toLowerCase());
                                    break;
                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                            } while (true);
                           
                        }else if (selection == 15){

                            do {
                                
                                try {
                                    System.out.println("\n\nThe selected computation is the Percentage Modulation of Both Systems.");
                                    System.out.println("The formula of this computation is %M = ("+"\u0394".toLowerCase()+"actual/"+"\u0394".toLowerCase()+"max) * 100");
                                    System.out.println("Where:\n %M is the Percentage Modulation");
                                    System.out.printf("%s is the actual frequency deviation of carrier signal (Hz)%n", "\u0394".toLowerCase());
                                    System.out.printf("%s is the actual maximum frequency deviation (Hz)%n", "\u0394".toLowerCase());

                                    System.out.print("Enter the actual deviation:");
                                    actualDeviationFrequency = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    actualDeviationFrequency = hertzConversion(selection, actualDeviationFrequency);

                                    System.out.print("Enter the maximum deviation frequency:");
                                    maximumDeviationFrequency = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    maximumDeviationFrequency = hertzConversion(selection, maximumDeviationFrequency);

                                    finalAnswer = computePercentageModulationIndex(actualDeviationFrequency, maximumDeviationFrequency);

                                    data = String.valueOf(finalAnswer);
                                    if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g %s%n", finalAnswer, "\u0394".toLowerCase());
                                    else
                                    System.out.printf("The answer is %.2f %s%n", finalAnswer, "\u0394".toLowerCase());
                                    break;

                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                            } while (true);
                        }else if(selection == 16){

                            do {
                                
                                try {
                                    System.out.println("\n\nThe selected computation is the Deviation Ratio of Both Systems.");
                                    System.out.println("The formula of this computation is DR = "+"\u0394".toLowerCase()+"max/Fm(max)");
                                    System.out.println("Where:\nDR is the Deviation Ratio");
                                    System.out.println("\u0394".toLowerCase()+"max is the maximum peak frequency deviation of the carrier signal (Hz)");
                                    System.out.println("Fm(max) is the maximum modulating frequency.");
                                    System.out.print("Enter the maximum peak frequency deviation:");
                                    maximumDeviationFrequency = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    maximumDeviationFrequency = hertzConversion(selection, maximumDeviationFrequency);

                                    System.out.print("Enter the maximum modulating frequency:");
                                    modulatingFrequency = scanner.nextDouble();

                                    System.out.print("do you need conversion?[y/n]:");
                                    choice = scanner.next().toLowerCase().charAt(0);
                                    
                                    if(choice == 'y')
                                    modulatingFrequency = hertzConversion(selection, modulatingFrequency);

                                    finalAnswer = computeDeviationRatio(maximumDeviationFrequency, modulatingFrequency);

                                    data = String.valueOf(finalAnswer);
                                    if(data.contains("E"))
                                    System.out.printf("The Answer is %.3g %s%n", finalAnswer, "DR");
                                    else
                                    System.out.printf("The answer is %.2f %s%n", finalAnswer, "DR");
                                    break;

                                } catch (Exception e) {
                                    scanner.nextLine();
                                    System.out.println("Error Occured Try Again");
                                }
                            } while (true);
                        }

                    System.out.print("Do you want to try again?[y/n]:");
                    tryAgain = scanner.next().toLowerCase().charAt(0);
                    if(tryAgain == 'n')
                    break;
                } catch (Exception e) {
                    scanner.nextLine();
                }
            }while(true);        
        
       
       scanner.close();
       System.out.println("---END---");
       System.out.println("Thank you for using my code. :)");
        }
        
        
        private static double computeSNR(double voltSignal, double voltNoise, int selection){
        
        double temp = 0.0;
        if(selection == 4){
        
            if(choice == 'n')
            temp = voltSignal / voltNoise;
            else if(choice == 'y')
            temp = (double)20*Math.log10((voltSignal/voltNoise));
        }else{
            temp = voltSignal / voltNoise;
            if(choice == 'y')
                temp = (Math.log10((voltSignal/voltNoise)) * 10);
        else
            System.out.println("Error Invalid input");
        }
         return temp;
        }
        
        private static double computeShotNoise(double current, double bandwidth){
        
        
        return Math.sqrt((2*I*current*bandwidth));
        }
        
        private static double computeInternalVoltageNoise(double kelvinTemp, double bandwidth, double resistance){
            
             return Math.sqrt((4*K*kelvinTemp*bandwidth*resistance));
        
        
        }
        
        private static double computeInternalPowerNoise(double kelvinTemp, double bandwidth){
        
        
        return K*kelvinTemp*bandwidth;
        }
        
        private static double toHertz(char type,double data){
        
        if(type == 'A')
        return data * Math.pow(10, -18);
        else if(type == 'F')
        return data * Math.pow(10, -15);
        else if(type == 'P')
        return data * Math.pow(10, -12);
        else if(type == 'N')
        return data * Math.pow(10, -9);
        else if(type == 'm')
        return data * Math.pow(10, -6);
        else if(type == 'M')
        return data * Math.pow(10, -3);
        else if(type == 'C')
        return data * Math.pow(10, -2);
        else if(type == 'd')
        return data * Math.pow(10, -1);
        else if(type == 'D')
        return data * Math.pow(10, 1);
        else if(type == 'H')
        return data * Math.pow(10, 2);
        else if(type == 'K')
        return data * Math.pow(10, 3);
        else if(type == '1')//MegaHertz has similar first character Micro and Milli and there's no free characters to it so select 1
        return data * Math.pow(10, 6);
        else if(type == 'G')
        return data * Math.pow(10, 9);
        else if(type == 'T')
        return data * Math.pow(10, 12);
        else if(type == 'P')
        return data * Math.pow(10, 15);
        else if(type == 'E')
        return data * Math.pow(10, 18);
        
        System.out.println("Invalid Input..");//this statement will run when type was not in the multipleSelections
        return data;
        }
        
        //this function provides validation for hertzConversion Conversion to Hertz
        private static boolean isTrue(char selectedSI){
        
        for(String s : SIunits){
        if(selectedSI == s.charAt(0))
        return true;
        
        }
        System.out.println("False");
        return false;

}

static double tempConversion(char choice,double temp){

        double data = 0.0;

        if(choice == 'F'){
        
        data = 5*(temp - 32)/9;//F conversion
        temp = data + 273;//C to Kelvin
        System.out.println("F to C to K :"+ temp);
        }else if(choice == 'C'){
        
        temp = temp + 273;
        System.out.println("C to K :"+ temp);
        }else
        System.out.println("Skipping Conversion.");
        return temp;

}

//this function need polyMorphism because this function needs different impelementations
static double hertzConversion(int selection, double frequency){

    char selectedHertz;
    try{
    

        do{
            //this selection executes if shot noise was selected. because shot noise only required Amps and need to modify the Array Conversion String
            if(selection == 3){
            System.out.println("Conversion in Amps and Hertz have the same Units of measure");
        
            for(String s : SIunits){
        
                System.out.printf("For %-10s Type '%1c'\n", s.replaceAll("Hertz", "Amps"), s.charAt(0));
            
        
            }
            //this selection only occurs if shot noise was not selected or shot noise is selected but the next user input resets the selection number. (see selection==3)
            }else if(selection == 11){
                for (String s : SIunits) {

                System.out.printf("For %-10s Type '%1c'\n", s.replaceAll("Hertz", "Volts"), s.charAt(0));
                
                }
            }else
                for (String s : SIunits) {
                System.out.printf("For %-10s Type '%1c'\n", s, s.charAt(0));
            
                }
        
            System.out.print("Enter your selected conversion:");
            selectedHertz = scanner.next().toUpperCase().charAt(0);
            //check if the function returns true
            if(isTrue(selectedHertz)){
                if(selectedHertz == 'M' | selectedHertz == 'D'){
             
                    boolean exit = false;//this boolean checks if the user can exit to the loop
                    do{

                    if(selectedHertz == 'M'){
                        if(selection == 3)
                            System.out.print("M was Selected Type M for MilliAmps, otherwise type m for MicroAmps:");
                        else
                            System.out.print("M was Selected Type M for MilliHertz, otherwise type m for MicroHertz:");
                        selectedHertz = scanner.next().charAt(0);

                        if(selectedHertz == 'M' || selectedHertz == 'm'){
             
                            if(selectedHertz == 'M'){
                                if(selection == 3)
                                    System.out.print("Pick what conversion type.\nType M for MilliAmps otherwise Type 1 for MegaAmps:");
                                else
                                    System.out.print("Pick what conversion type.\nType M for MilliHertz otherwise Type 1 for MegaHertz:");
                                selectedHertz = scanner.next().charAt(0);
                            }
                            frequency = toHertz(selectedHertz, frequency);
                            return frequency;
                            
                        }else{
            
                            System.out.println("Invalid try again");
                        }
                }else if(selectedHertz == 'D'){
                    if(selection == 3)
                        System.out.print("D was Selected Type D for DecaWatts, otherwise type d for DeciWatts:");
                    else
                        System.out.print("D was Selected Type D for DecaHertz, otherwise type d for DeciHertz:");
                    selectedHertz = scanner.next().charAt(0);
                    if(selectedHertz == 'D' | selectedHertz == 'd'){
                        frequency = toHertz(selectedHertz, frequency);
                        return frequency;
                    }else{
             
                        System.out.println("Invalid try again");
                    }
                }
                }while(!exit);
            
            }
                frequency = toHertz(selectedHertz,frequency);
                choice = 'n';
            }else{
            System.out.print("Please check your input do you want to try again?:[y/n]");
            choice = scanner.next().charAt(0);
            }
        
        
        }while(choice == 'y');
        
        
        }catch(Exception e){
        
        e.printStackTrace();
        
        }
        
         System.out.println("Converted hertz are:"+ frequency);
         return frequency;

}

static double computeNoiseTemperature(double noiseRation){


    return 290*(noiseRation - 1);//computation of this are noiseRatio -1 and compute * noiseRatio

}

static double computeModulatingIndex(double vMax, double vMin){


return (vMax-vMin)/(vMax+vMin);//Compute the values of on the parenthesis and divide it and return to the caller
}

static double computeSideBands(double frequencyCarrier, double frequencyModulation, char choice){

if(choice == '1')//this selection will only executed when the choice is selected to 1 otherwise user wants to compute to lsb
    return frequencyCarrier + frequencyModulation;
    else
    return frequencyCarrier - frequencyModulation;
}

//This function provides computation for Overall Noise Performance it cannot take arguments because there's no definitive finite steps how to calculate the computation
static double computeOverallNF(){

	//Total Noise Ration 
    double noiseRT = 0.0;
    
    double[] noiseRatio = new double[10];//stores NR data
    
    double[] amplitudes = new double[10];//stores An data
    
    double[] A = new double[10];//this array stores the multiplicative computations of each An
    
    double[] noiseComputation = new double[10];//this array stores the computed divide of each data;
    
    double temp = 0.0;//to store the whole addition of the computation 
    
    int count = 0, count2 = 1;//this two variable represents the limit of the inputs, count2 has increased by 1 to compute the NR2 without going to NR1
    
    char addMore = 'y';// for the do while loop control
    do{
    
    try{
    
    System.out.println("The Selected Computation is NRT the Formula of this equation is");
    System.out.println("NRT = NR1 + (NR2-1/A1)+(NR3 -1/A1A2 +....)");
    System.out.println("Note: this feature is not well trimmed yet so be careful using this feature.\nThis feature can only handle 10 inputs.");
    
    //this do while loop request data from the user to compute the computation
    do{

    	//this selection checks wether the user exceeded the maximum inputs that can carry of the function
     if(count==9){

     	System.out.println("Exceeded Maximum inputs processing now...");
     	break;

     }
	System.out.println("Input count :"+ (count+1));
    System.out.printf("Enter the NR%d figure:", (count+1));
    noiseRatio[count] = scanner.nextDouble();
    System.out.println("input :" + noiseRatio[count]);
    System.out.printf("Enter the A%d:", (count+1));
    amplitudes[count] = scanner.nextDouble();
    System.out.println("input :" + amplitudes[count]);
    System.out.print("Add more?[y/n]:");
    addMore = scanner.next().toLowerCase().charAt(0);
    if(addMore == 'y')
    count++;

    }while(addMore != 'n');

    /*
        This whole Algorithm performs the three parts of the computation
        1st. Multiply all the A(n)
        2nd. Divide the NR(n)-1/A(n)
        3rd. Add the (NR(n)-1/A(n) to NR1)
    */
    A[0] = amplitudes[0];
    if(amplitudes[1] != 0)
    A[1] = amplitudes[0] * amplitudes[1];
    
    if(amplitudes[2] != 0)
    A[2] = amplitudes[0] * amplitudes[1] * amplitudes[2];
    
    if(amplitudes[3] != 0)
    A[3] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3];
    
    if(amplitudes[4] != 0)
    A[4] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4];
    
    if(amplitudes[5] != 0)
    A[5] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4] * amplitudes[5];
    
    if(amplitudes[6] != 0)
    A[6] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4] * amplitudes[5] * amplitudes[6];
    
    if(amplitudes[7] != 0)
    A[7] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4] * amplitudes[5] * amplitudes[6] * amplitudes[7];
    
    if(amplitudes[8] != 0)
    A[8] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4] * amplitudes[5] * amplitudes[6] * amplitudes[7] * amplitudes[8];
    
    if(amplitudes[9] != 0)
    A[9] = amplitudes[0] * amplitudes[1] * amplitudes[2] * amplitudes[3] * amplitudes[4] * amplitudes[5] * amplitudes[6] * amplitudes[7] * amplitudes[8] * amplitudes[9];
    

    
    //this loop computes the division of the whole NRn-1/An an store it to the noiseComputation array
    for(int j=0; j<count; j++){
    	
    	noiseComputation[j] = (noiseRatio[count2]-1)/A[j];//count2 has an index to 1 so it doesnt get the NR1 figure
    	count2++;

    }
    temp = noiseRatio[0];//this expression assigned the noiseRation index 0 to the temp variable to avoid adding it to the noiseComputation array;
    for(int i=0; i<count; i++){
    
    	
        temp = temp + noiseComputation[i];
    
    }
    
    
    break;
    }catch(Exception e){
    e.printStackTrace();
    scanner.nextLine();
    e.printStackTrace();
    }
    break;
    }while(true);

    //passing value to NRT
    noiseRT = temp;
    return noiseRT;//return the value to the main caller.
}

static double computeFMModulatingIndex(double frequencyDeviation, double modulationFrequency){


    return (frequencyDeviation/modulationFrequency);
}

static double computedeviationSensitivity(double deviationSensitivity, double peakMaxVoltage){


return (deviationSensitivity*peakMaxVoltage);
}
static double computeFMBandwidth(double frequencyDeviation, double modulationFrequency){


    return 2*(frequencyDeviation+modulationFrequency);
}

static double computePhaseModulatingFrequency(double deviationSensitivity, double modulationFrequency){

    return (deviationSensitivity*modulationFrequency);
}

static double computedeviationSensitivity(double deviationSensitivity, double modulatingFrequency, double peakMaxVoltage){


    return (deviationSensitivity*peakMaxVoltage*modulatingFrequency);
}

static double computePercentageModulationIndex(double actualDeviationFrequency, double maximumDeviationFrequency){

    return (actualDeviationFrequency/maximumDeviationFrequency)*100;
}

static double computeDeviationRatio(double peakFrequencyDeviation, double peakModulatingFrequency){


    return (peakFrequencyDeviation/peakModulatingFrequency);
}
}
